<?php

return [
    'server' => [
        'host' => '',       // 服务监听的 IP
        'port' => '',       // 服务监听的 端口
    ],
    'projectBase' => '/www/webs/',         // 项目组目录
    'accessSecret' => 'xxxxxxxxxxxxxxx',   // 签名秘钥
    'logPath' => 'runtime/log',            // 日志打印目录
    'sender' => [                                // 消息推送配置
        'class' => \App\sender\Dingding::class,  // 将结果推送到钉钉
        'params' => [                            // 钉钉 webhook 相关配置
            'webhook' => 'xxxxxxx',     // 钉钉 webhook 请求链接
            'secret' => 'xxxxxxxx',     // 钉钉 webhook 请求签名
        ]
    ]
];
